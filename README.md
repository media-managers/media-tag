#  Media-Tag
I enjoy downloading and listening to live shows from [The Grateful Dead](https://archive.org/details/GratefulDead) 
as well as archiving my CD Collection. I also like to keep my files organized. 

## Info
I store the albums in directories with the date and location or title:
```
$../Music/JamBands/Grateful Dead/Live Shows/1972-08-27 - Sunshine Daydream - Veneta, OR
$../Music/Progressive Rock/Camel/1974 - Mirage
```
Songs are named accordingly:
```
01 - SongName.flac
02 - SongName.flac
...
```

I prefer to keep multi-disc albums in one directory with the tracks numbered in sequence. The id3/VORBIS
tags will keep track of the disc that the songs were on should I ever need to burn a disc.

I use my other [scripts](https://gitlab.com/chuckn246/renumber) (addash, dotodash, renumber) to modify the track numbers.

After that, I run this script from the album's folder to tag the files according to the file names and directory name. 

I keep the script symlinked to `/usr/local/bin` so I can run it from anywhere.

## What it do
1. It asks the Artist name for **Album Artist** and **Artist**.
2. It asks the Genre for **Genre**
3. It the asks how many discs for **Total discs** (Currently max 4)
4. If more than 1 disc, it'll ask what track Disc 2+ starts.
5. It strips existing id3/VORBIS tags. 
6. It gets the current directory then: 
    1. It strips the year and uses that for **Album** for studio albums.
    2. It uses the whole directory name for **Album** for live albums. 
7. It uses the first 4 characters of the current directory for the **Year**.
8. It uses the filename, strips the track number and file extension for the **Track Name**.
9. It uses the first 2 characters of the filename for the **Track Number**.
10. It counts the {flac,mp3} files and uses that for the Total **Track Number**.

## What you need

**tagflac** uses `metaflac` to set the VORBIS tags

`metaflac` is included in the `flac` package:
```
sudo apt install flac
```
**tagmp3** uses [eyeD3](http://eyed3.nicfit.net/) to set the id3 tags.

I'm on Debain so I installed it by typing in console:
```
sudo apt install eyed3
```

DISCLAIMER: 
Use with caution. Please don't blame me if you botch your media collection!
