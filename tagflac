#!/usr/bin/env bash

# Name: tagflac
# Path: /usr/local/bin/
# Desc: This script will loop through the flac files in 
# the current directory and set the meta tags on flac files 
# using 'metaflac'. 
# The tags are in Vorbis comment spec. 

# Here's a list of the ones that it will use:

# ALBUM
# ALBUM ARTIST
# ALBUMARTIST
# ARTIST
# DATE
# DISC
# DISCNUMBER
# DISCTOTAL
# GENRE
# ISRC - Not sure if I'll use it.
# TITLE
# TOTALDISC
# TOTALTRACKS
# TRACKNUMBER
# TRACKTOTAL
# YEAR

# I may attempt to find a way to implement these in the future:

# MUSICBRAINZ_ALBUMARTISTID
# MUSICBRAINZ_TRACKID
# MUSICBRIANZ_ARTISTID
# MUSICBRAINZ_DISCID

#
# DISC TAGGING FUNCTIONS
#

one() {
    for (( l=1; l<=tracks; l++ )); do
    printf -v numl "%02d" "${l}";
    metaflac --set-tag="DISC=1" \
    --set-tag="DISCNUMBER=1" "${numl}"*.flac;
done
}

oneplus() {
    for (( l=1; l<disc2; l++ )); do
    printf -v numl "%02d\n" "${l}"
    metaflac --set-tag="DISC=1" \
    --set-tag="DISCNUMBER=1" "${numl}"*.flac;
done
}

two() {
    for (( m=disc2; m<=tracks; m++ )); do
    printf -v numm "%02d\n" "${m}"
    metaflac --set-tag="DISC=2" \
    --set-tag="DISCNUMBER=2" "${numm}"*.flac;
done
}

twoplus() {
    for (( m=disc2; m<disc3; m++ )); do
    printf -v numm "%02d\n" "${m}"
    metaflac --set-tag="DISC=2" \
    --set-tag="DISCNUMBER=2" "${numm}"*.flac;
done
}

three() {
    for (( n=disc3; n<=tracks; n++ )); do
    printf -v numn "%02d\n" "${n}"
    metaflac --set-tag="DISC=3" \
    --set-tag="DISCNUMBER=3" "${numn}"*.flac;
done
}

threeplus() {
    for (( n=disc3; n<disc4; n++ )); do
    printf -v numn "%02d\n" "${n}"
    metaflac --set-tag="DISC=3" \
    --set-tag="DISCNUMBER=3" "${numn}"*.flac;
done
}

four() {
    for (( o=disc4; o<=tracks; o++ )); do
    printf -v numo "%02d\n" "${o}"
    metaflac --set-tag="DISC=4" \
    --set-tag="DISCNUMBER=4" "${numo}"*.flac;
done
}


# Set variables

# Get directory name. 
# Should be in the following format: 
# "YYYY - AlbumName" for Studio/Compilation Albums 
# "YYYY-MM-DD - Venue - City,ST" for Live Shows
directory="${PWD##*/}"

# Get Year 
yeardate="${directory:0:4}"

# Check the format of the date
fulldate="^[0-9]{4}-[0-9]{2}-[0-9]{2} -"
justyear="^[0-9]{4} -"

# Set album title depending on Albumname format
if [[ "${directory}" =~ ${justyear}* ]]; then
    albumtitle="${directory:7}";
elif [[ "${directory}" =~ ${fulldate}* ]]; then
    albumtitle="${directory}";
# Could probably do without the elif.. Leaving
# like this just in case I have another exception.
else albumtitle="${directory}";
fi

# Get total number of tracks.
tracks=$(find . -maxdepth 1 -name '*.flac' | wc -l)

# Ask Artist name
read -rp "Artist Name? " artist

# Ask Album name. Currently overridden by the directory name.
#read -rp "Album? " album

# Ask Genre.
# A menu would be cool to have so we have consistent genre names.
read -rp "Genre? " genre

# Ask how many discs. 
read -rp "How many discs?(Max 4) " discs
    while [[ ! ${discs} =~ ^[0-9]+$ ]]; do
       read -rp "4 disc maximum!" discs
       ! [[ ${discs} -ge 1 && ${discs} -le 4 ]] && unset discs
    done

# Ask the starting track for each disc.
if [ "$discs" -eq 2 ]; then
    unset disc2
    while [[ ! ${disc2} =~ ^[0-9]+$ ]]; do
       read -rp "Please enter the starting track for Disc 2: " disc2
       ! [[ ${disc2} -gt 1 && ${disc2} -le ${tracks} ]] && unset disc2
    done
elif [ "$discs" -eq 3 ]; then
    unset disc2
    while [[ ! ${disc2} =~ ^[0-9]+$ ]]; do
       read -rp "Please enter the starting track for Disc 2: " disc2
       ! [[ ${disc2} -gt 1 && ${disc2} -lt ${tracks} ]] && unset disc2
    done
    unset disc3
    while [[ ! ${disc3} =~ ^[0-9]+$ ]]; do
       read -rp "Please re-enter the starting track for Disc 3: " disc3
       ! [[ ${disc3} -gt ${disc2} && ${disc3} -le ${tracks} ]] && unset disc3
    done
elif [ "$discs" -eq 4 ]; then
    unset disc2
    while [[ ! ${disc2} =~ ^[0-9]+$ ]]; do
       read -rp "Please re-enter the starting track for Disc 2: " disc2
       ! [[ ${disc2} -gt 1 && ${disc2} -le ${tracks} ]] && unset disc2
    done
    unset disc3
    while [[ ! ${disc3} =~ ^[0-9]+$ ]]; do
       read -rp "Please re-enter the starting track for Disc 3: " disc3
       ! [[ ${disc3} -gt ${disc2} && ${disc3} -lt ${tracks} ]] && unset disc3
    done
    unset disc4
    while [[ ! ${disc4} =~ ^[0-9]+$ ]]; do
       read -rp "Please re-enter the starting track for Disc 4: " disc4
       ! [[ ${disc4} -gt ${disc3} && ${disc4} -le ${tracks} ]] && unset disc4
    done
else
    discs=1
fi

# Loop through the current directory and set tags.
for f in *.flac; do
    title="${f:5}" # Strip leading numbers from filename
    metaflac --remove-all-tags "${f}"; # Wipe the existing metadata
    metaflac --set-tag="ALBUM=${albumtitle}" \
    --set-tag="ALBUM ARTIST=${artist}" \
    --set-tag="ALBUMARTIST=${artist}" \
    --set-tag="ARTIST=${artist}" \
    --set-tag="DATE=${yeardate}" \
    --set-tag="DISCTOTAL=${discs}" \
    --set-tag="GENRE=${genre}" \
    --set-tag="TITLE=${title%.*}" \
    --set-tag="TOTALTRACKS=${tracks}" \
    --set-tag="TRACKNUMBER=${f:0:2}" \
    --set-tag="TRACKTOTAL=${tracks}" \
    --set-tag="YEAR=${yeardate}" "${f}" # Use the year from the directory name
done


# Write Disc tags

# Disc One
if [ "${discs}" -gt 1 ]; then
oneplus
else
one
fi

# Disc Two
if [ "${discs}" -eq 2 ]; then 
two
elif [ "${discs}" -gt 2 ]; then
twoplus
fi

# Disc Three
if [ "${discs}" -eq 3 ]; then
three
elif [ "${discs}" -gt 3 ]; then
threeplus
fi

# Disc Four
if [ "${discs}" -eq 4 ]; then
four
fi

